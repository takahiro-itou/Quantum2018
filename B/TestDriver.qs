
namespace  Solution  {

open  Microsoft.Quantum.Canon;
open  Microsoft.Quantum.Diagnostics;
open  Microsoft.Quantum.Intrinsic;

operation  Test1() : () {
    body {
        using (register = Qubit[2]) {
            DumpMachine("dump-1-initial.txt");

            Solve(register, 0);
            DumpMachine("dump-1-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test2() : () {
    body {
        using (register = Qubit[2]) {
            DumpMachine("dump-2-initial.txt");

            Solve(register, 1);
            DumpMachine("dump-2-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test3() : () {
    body {
        using (register = Qubit[2]) {
            DumpMachine("dump-3-initial.txt");

            Solve(register, 2);
            DumpMachine("dump-3-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test4() : () {
    body {
        using (register = Qubit[2]) {
            DumpMachine("dump-4-initial.txt");

            Solve(register, 3);
            DumpMachine("dump-4-result.txt");
            ResetAll(register);
        }
    }
}

}
