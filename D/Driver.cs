using   System;

using   Microsoft.Quantum.Simulation.Core;
using   Microsoft.Quantum.Simulation.Simulators;

namespace  Solution  {

class  Driver
{

    static  void  Main(string[] args)
    {
        using (var qsim = new QuantumSimulator()) {
            Console.Write("Running Test1... ");
            var result1 = Test1.Run(qsim).Result;
            Console.WriteLine(result1);
        }

        using (var qsim = new QuantumSimulator()) {
            Console.Write("Running Test2 ... ");
            var result2 = Test2.Run(qsim).Result;
            Console.WriteLine(result2);
        }

    }
}

}
