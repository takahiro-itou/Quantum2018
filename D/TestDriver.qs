
namespace  Solution  {

open  Microsoft.Quantum.Canon;
open  Microsoft.Quantum.Diagnostics;
open  Microsoft.Quantum.Intrinsic;

operation  Test1() : Int {
    body {
        using (register = Qubit[1]) {
            let q = register[0];
            H(q);
            DumpMachine("dump-1-initial.txt");

            let result  = Solve(q);
            DumpMachine("dump-1-after.txt");
            Reset(q);

            return ( result );
        }
    }
}

operation  Test2() : Int {
    body {
        using (register = Qubit[1]) {
            let q = register[0];
            X(q);
            H(q);
            DumpMachine("dump-2-initial.txt");

            let result  = Solve(q);
            DumpMachine("dump-2-after.txt");
            Reset(q);

            return ( result );
        }
    }
}

}
