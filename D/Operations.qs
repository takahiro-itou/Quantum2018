
namespace  Solution  {

open  Microsoft.Quantum.Canon;
open  Microsoft.Quantum.Intrinsic;

operation  Solve(q: Qubit) : Int {
    body
    {
        H(q);
        if ( M(q) == Zero ) {
            return ( 1 );
        } else {
            return ( -1 );
        }
    }
}

}
