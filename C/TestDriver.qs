
namespace  Solution  {

open  Microsoft.Quantum.Canon;
open  Microsoft.Quantum.Diagnostics;
open  Microsoft.Quantum.Intrinsic;

operation  Test1() : () {
    body {
        using (register = Qubit[1]) {
            DumpMachine("dump-1-initial.txt");

            Solve(register);
            DumpMachine("dump-1-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test2() : () {
    body {
        using (register = Qubit[2]) {
            DumpMachine("dump-2-initial.txt");

            Solve(register);
            DumpMachine("dump-2-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test3() : () {
    body {
        using (register = Qubit[3]) {
            DumpMachine("dump-3-initial.txt");

            Solve(register);
            DumpMachine("dump-3-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test4() : () {
    body {
        using (register = Qubit[4]) {
            DumpMachine("dump-4-initial.txt");

            Solve(register);
            DumpMachine("dump-4-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test5() : () {
    body {
        using (register = Qubit[5]) {
            DumpMachine("dump-5-initial.txt");

            Solve(register);
            DumpMachine("dump-5-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test6() : () {
    body {
        using (register = Qubit[6]) {
            DumpMachine("dump-6-initial.txt");

            Solve(register);
            DumpMachine("dump-6-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test7() : () {
    body {
        using (register = Qubit[7]) {
            DumpMachine("dump-7-initial.txt");

            Solve(register);
            DumpMachine("dump-7-result.txt");
            ResetAll(register);
        }
    }
}

operation  Test8() : () {
    body {
        using (register = Qubit[8]) {
            DumpMachine("dump-8-initial.txt");

            Solve(register);
            DumpMachine("dump-8-result.txt");
            ResetAll(register);
        }
    }
}

}
